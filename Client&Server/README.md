Realizzazione di una chat privata tramite un server e client.


Versione 1:

Al suo avvio il client può connetersi al server inserendo le proprie credenziali.

L'utente può in qualsiasi momento terminare l'esecuzione scrivendo QUIT e inviando.

Una volta loggatosi può scegliere con chi scambiarsi messaggi.

Verrà visualizzata l'anteprima della chat scritta finora e l'utente sarà fornito di istruzioni su come ricevere nuovi messaggi.

All'invio vuoto da tastiera viene richiesto al server di cercare nuovi messaggi, questo deve essere fatto sia prima di mandare un nuovo messaggio, sia prima di chiudere il client.

Versione 2:

Al suo avvio il client può connetersi al server inserendo le proprie credenziali.

L'utente può in qualsiasi momento terminare l'esecuzione scrivendo QUIT e inviando.

Una volta loggatosi può scegliere con chi scambiarsi messaggi.

A differenza della versione 1 i messaggi vengono gestiti attraverso due thread che si occupano della recv e della send, garantendo ricezione in tempo reale senza il bisogno di terze istruzioni.
