#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <signal.h> 

#include "common.h"

using namespace std;

//dichiaro un array di N semafori dove N è 5
sem_t semaphores[N];

//dichiaro qui la socket del server in modo che possa chiuderla con il SIGINT e SIGTSTP
int master_socket;

unsigned int master_client_id = 0;

// Per gestire CTRL+C e CTRL+Z
void signal_callback_handler(int signum) {
   cout << "\nServer closed, bye! :)" << endl;
   // Terminate programs
   // Chiudo i semafori
   int ret;
   for(int i = 0; i < N; i++) {
       ret = sem_destroy(&semaphores[i]);
       if(ret) handle_error("Errore nella chiusura del semaforo");
    }
   //chiudo socket e esco
   ret = close(master_socket);
   if(ret) handle_error("Errore nella chiusura della socket del server");
   exit(signum);
}

//qui ho salvati i nomi dei file txt delle chat con i quali inizializzo poi le struct
std::string Chats[USERNAME_SIZE*2] = {"aaaadenrrs.txt", "aaaddeelnnoorr.txt", "aaadelnoopr.txt", "aaadelnoorrs.txt", "aaalooprs.txt",
				      "aaccdeilnnoor.txt", "aaccilnoors.txt", "aadellnoooopr.txt", "accdeillnnoooor.txt",
				      "accillnoooop.txt"};

//struct del thread
typedef struct handler_args_s {
    unsigned int client_id;
    int socket_desc;
    struct sockaddr_in* client_addr;
} handler_args_t;

// Usare la Struct per Database
struct CHAT{
    std::string chat_content;
    std::string chat_name;
};

struct username
{
    std::string name[USERNAME_SIZE];
    std::string password[USERNAME_SIZE];
    int log_flag[USERNAME_SIZE];
    CHAT chat[USERNAME_SIZE*2];
} username[USERNAME_SIZE];

std::string RemoveNewLine(std::string query){
	query.erase(std::remove(query.begin(), query.end(), '\n'), query.end());
	return query;
}

//questa funzione mi permette di vedere se l'utente ha solo premuto invio per fare il refresh di messaggi nuovi
bool is_nothing(std::string buf, std::string check_if_nothing){
	if(buf == check_if_nothing) return true;
	else return false;
}

//questa funzione mi trova l'indice dell'username nella struct
int GetNameIndex(std::string query)
{
	
	query.erase(std::remove(query.begin(), query.end(), '\n'), query.end());
	for (int i=0; i<USERNAME_SIZE; i++)
	{
		//std::cout << "Controllo: " << username->name[i] << "";
		if (query == username->name[i]){
		 return i; //Return the index
		}
		//printf("\n");
	}
	return -1; //Error code
}

//questa funzione controlla se la password inserita è giusta in base all'index ricavato dalla funzione precedente
bool PasswordMatches(int index, std::string passwd)
{
	passwd.erase(std::remove(passwd.begin(), passwd.end(), '\n'), passwd.end());
	return (passwd == username->password[index]);
}

//inizializzo la struct user
void InitializeUsers(string UsArray[], string PassArray[]){
	for (int i=0; i<USERNAME_SIZE; i++){
		username->name[i]=UsArray[i];
		username->password[i]=PassArray[i];
		username->log_flag[i]=0;
		printf(".");
	}
}

//inizializzo la struct chat
void InitializeChats(std::string ChatsArray[]){
	for (int i=0; i<USERNAME_SIZE*2; i++){
	username->chat[i].chat_name = ChatsArray[i];

        std::ifstream file(username->chat[i].chat_name);
        std::string str;
        std::string file_contents;
    
        while (std::getline(file, str))
        {
  		file_contents += str;
  		file_contents.push_back('\n');
        }

	username->chat[i].chat_content = file_contents;	
	printf(".");
	file.close();
    }
}

//funzione utilizzata nel loop della chat per salvare il contenuto nella struct
void save_chat_on_struct(std::string chat_content, std::string chat_name, int client_id){
int ret;
	for (int i=0; i<USERNAME_SIZE*2; i++){
	if (chat_name == username->chat[i].chat_name){
		ret = sem_wait(&semaphores[client_id]);
		if (ret) handle_error("Errore wait del semaforo nel save_chat_on_struct");
		username->chat[i].chat_content = chat_content;
		printf("Saved chat on struct...\n");
    		ret = sem_post(&semaphores[client_id]);
		if (ret) handle_error("Errore post del semaforo nel save_chat_on_struct");
	}
    }
}

//funzione utilizzata nel loop della chat per salvare il contenuto nei file
void save_chat_on_file(std::string chat_name, std::string chat_content, int client_id){
int ret;
	for (int i=0; i<USERNAME_SIZE*2; i++){
	if(chat_name == username->chat[i].chat_name){
    		ret = sem_wait(&semaphores[client_id]);
		if (ret) handle_error("Errore wait del semaforo nel save_chat_on_file");
		std::ofstream file(chat_name, std::ofstream::trunc);
		file << chat_content;
        	file.flush();
		file.close();	
		ret = sem_post(&semaphores[client_id]);
		if (ret) handle_error("Errore post del semaforo nel save_chat_on_file");
	}
    }
}

//funzione che mi controlla se il nome utente esiste nel "database"
bool userexist(std::string user){
	user.erase(std::remove(user.begin(), user.end(), '\n'), user.end());
	for (int i=0; i<USERNAME_SIZE; i++){
		if (user == username->name[i]) return true;
	}
	return false;
}

//questa funzione mi consente di trovare il nome della chat esenguendo un sort tra i due nomi passati e aggiungendo .txt alla fine
std::string getchatname(std::string sender, std::string receiver){
	sender.erase(std::remove(sender.begin(), sender.end(), '\n'), sender.end());
	receiver.erase(std::remove(receiver.begin(), receiver.end(), '\n'), receiver.end());
	std::string final_name = sender+receiver;
	std::sort(final_name.begin(), final_name.end());
	final_name+=".txt";
	return final_name;
}

//questa funzione restituisce il contenuto della chat
std::string getChat(std::string chat_name){
	std::string chat_content;
	for (int i=0; i<USERNAME_SIZE*2; i++){
		if(chat_name == username->chat[i].chat_name){
			chat_content = username->chat[i].chat_content;
			return chat_content;
		}
    }
    chat_content = "Error";
    return chat_content;
}

//funzioni di encrypt e decrypt stesse del client
void Encrypt(char *str, unsigned int client_id){
int msg_len = strlen(str);
for(int i = 0; (i < msg_len-1); i++)
            str[i] = str[i] + 2; //the key for encryption is 3 that is added to ASCII value
	 printf("\n[CLIENT #%u]", client_id+1);
         cout << " Encrypted string... " << endl;
}

void Decrypt(char *str, unsigned int client_id){
int msg_len = strlen(str);
for(int i = 0; (i < msg_len-1); i++)
         str[i] = str[i] - 2; //the key for encryption is 3 that is subtracted to ASCII value
      printf("\n[CLIENT #%u]", client_id+1);
      cout << " Decrypted string..." << endl;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void* connection_handler(void* arg) {

    int ret, recv_bytes, bytes_sent;
    
    handler_args_t* args = (handler_args_t*)arg;
    int socket_desc = args->socket_desc;
    struct sockaddr_in* client_addr = args->client_addr;
    unsigned int client_id = args->client_id;

    char buf[1024];
    char* quit_command = (char*)SERVER_COMMAND;
    size_t quit_command_len = strlen(quit_command);

    // parso il client IP address e la porta
    char client_ip[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &(client_addr->sin_addr), client_ip, INET_ADDRSTRLEN);
    uint16_t client_port = ntohs(client_addr->sin_port); // port number is an unsigned short
    printf("Client %u connected on port %u \n", client_id+1, client_port);

    int msg_len = strlen(buf);

    size_t buf_len = sizeof(buf);
    int successlogin, successusername;
    char usernamecheck[50];
    char saved_receiver[50];
    size_t usernamecheck_len = sizeof(usernamecheck);
    size_t saved_receiver_len = sizeof(saved_receiver);
    std::string chat_content, temp_chat_content;
    int chat_length, chat_length_temp;

    // mando il messaggio di benvenuto
    sprintf(buf, "[WELCOME]\nHi! I'm the server.\nI will need your login credential first.\n"
            "I will stop if you send me %s", quit_command);

    msg_len = strlen(buf);

    bytes_sent=0;
	while ( bytes_sent < msg_len) {
        ret = send(socket_desc, buf + bytes_sent, msg_len - bytes_sent, 0);
        if (ret == -1 && errno == EINTR) continue;
        if (ret == -1) handle_error("Cannot write to the socket");
        bytes_sent += ret;
    }

    if (DEBUG) fprintf(stderr, "[CLIENT #%u] Welcome message has been sent\n\n", client_id+1);
    
    //Control Loop
    fprintf(stderr, "[CLIENT #%u] Server entered LOGIN LOOP...\n", client_id+1);

    while(1){
	successlogin = 0;
        memset(&buf,0,buf_len);
	recv_bytes = 0;
        do {
            ret = recv(socket_desc, buf + recv_bytes, 1, 0);
            if (ret == -1 && errno == EINTR) continue;
            if (ret == -1) handle_error("Cannot read from the socket");
            if (ret == 0) break;
		} while ( buf[recv_bytes++] != '\n' );

	Decrypt(buf, client_id);

	printf("[CLIENT #%u] Received this username: %s", client_id+1, buf);

	//Controlla se ho ricevuto QUIT
	if (recv_bytes == (int)quit_command_len && !memcmp(buf, quit_command, quit_command_len)){

        if (DEBUG) fprintf(stderr, "[CLIENT #%u] Received QUIT command...\n", client_id+1);
             goto L;
         }

	int index;
	
	//Controllo se l'username esiste nel database
	index = GetNameIndex(buf);

	//Mi salvo l'username per controlli successivi
	memset(&usernamecheck,0,usernamecheck_len);
	strcpy(usernamecheck, buf);

	if(index != -1){		

		memset(&buf,0,buf_len);
		fprintf(stderr, "[CLIENT #%u] Ok the server found the username now it's going to ask for the password...\n", client_id+1);
		sprintf(buf, "Insert your password:\n");

		Encrypt(buf, client_id);

		msg_len = strlen(buf);

   		bytes_sent=0;
		while ( bytes_sent < msg_len) {
        		ret = send(socket_desc, buf + bytes_sent, msg_len - bytes_sent, 0);
        		if (ret == -1 && errno == EINTR) continue;
        		if (ret == -1) handle_error("Cannot write to the socket");
        		bytes_sent += ret;
    		}

		memset(&buf,0,buf_len);
		recv_bytes = 0;
        	do {
            	ret = recv(socket_desc, buf + recv_bytes, 1, 0);
            	if (ret == -1 && errno == EINTR) continue;
            	if (ret == -1) handle_error("Cannot read from the socket");
            	if (ret == 0) break;
			} while ( buf[recv_bytes++] != '\n' );


		Decrypt(buf, client_id);

		//solito controllo se ho ricevuto QUIT
		if (recv_bytes == (int)quit_command_len && !memcmp(buf, quit_command, quit_command_len)){

        	if (DEBUG) fprintf(stderr, "[CLIENT #%u] Received QUIT command...\n", client_id+1);
             		goto L;
         	}

		char passwordprint[50];
		strcpy(passwordprint, buf);

		std::string passwordprint2 = RemoveNewLine(passwordprint);

		//printf("[CLIENT #%u]", client_id+1);
		std::cout << "[CLIENT #" << client_id+1 << "] Password received is: *" << passwordprint2 << "* checking if password is correct\n";

		bool passwordcheck;

		//controllo se la password è giusta
		passwordcheck = PasswordMatches(index,buf);		

		//printf("PROVA LOGGED: %s\n", check_user_logged ? "true" : "false");
		//if(passwordcheck == true && check_user_logged == false){

		if(passwordcheck == true){
			memset(&buf,0,buf_len);
			fprintf(stderr, "[CLIENT #%u] Password Successful for user with index %d\n", client_id+1, index);
			sprintf(buf, "Login Successful\n");
			successlogin = 1;
		}
		else{
        	memset(&buf,0,buf_len);
			fprintf(stderr, "[CLIENT #%u] Invalid Password...\n", client_id+1);
			sprintf(buf, "Invalid Password\n");
		}
	}
	else{
        memset(buf,0,buf_len);
	fprintf(stderr, "[CLIENT #%u] Invalid Username...\n", client_id+1);
	sprintf(buf, "Invalid Username\n");
	}

	Encrypt(buf, client_id);

	msg_len = strlen(buf);

	//mando il risultato dei controlli precedenti
        bytes_sent=0;
        while ( bytes_sent < msg_len) {
            ret = send(socket_desc, buf + bytes_sent, msg_len - bytes_sent, 0);
            if (ret == -1 && errno == EINTR) continue;
            if (ret == -1) handle_error("Cannot write to the socket");
            bytes_sent += ret;
        }

	if (successlogin == 1) break;		
	else {printf("[CLIENT #%u] Checking again...\n", client_id+1);}
    }

	sleep(1);

        //Controllo Receiver
	printf("[CLIENT #%u] Now I'm going to ask the user with who does he want to communicate...\n", client_id+1);

    while(1){
        //mando al client il messaggio di richiesta del destinatario
	memset(&buf,0,buf_len);
	successusername = 0;
	sprintf(buf, "With who do you want to communicate?\n");

	Encrypt(buf, client_id);

	msg_len=strlen(buf);

	bytes_sent=0;
        while ( bytes_sent < msg_len) {
            ret = send(socket_desc, buf + bytes_sent, msg_len - bytes_sent, 0);
            if (ret == -1 && errno == EINTR) continue;
            if (ret == -1) handle_error("Cannot write to the socket");
            bytes_sent += ret;
        }

	if (DEBUG) fprintf(stderr, "[CLIENT #%u] Sent message of %d bytes...\n", client_id+1, bytes_sent);

	memset(&buf,0,buf_len);

	recv_bytes = 0;
        do {
            ret = recv(socket_desc, buf + recv_bytes, 1, 0);
            if (ret == -1 && errno == EINTR) continue;
            if (ret == -1) handle_error("Cannot read from the socket");
            if (ret == 0) break;
	   } while ( buf[recv_bytes++] != '\n' );

	Decrypt(buf, client_id);

	//solito controllo di QUIT
	if (recv_bytes == (int)quit_command_len && !memcmp(buf, quit_command, quit_command_len)){

        if (DEBUG) fprintf(stderr, "[CLIENT #%u] Received QUIT command...\n", client_id+1);
             goto L;
         }

	if (strcmp(buf, usernamecheck) == 0){
	//controllo se si è inserito lo stesso nome del mittente
	printf("[CLIENT #%u] Cannot chat with yourself...\n", client_id+1);
	memset(&buf,0,buf_len);	
	sprintf(buf, "You cannot chat with yourself\n");
	}
	else{
	//controllo se esiste l'user nel database
	if(userexist(buf) == true){
		// mi salvo il ricevente per controlli successivi
		memset(&saved_receiver,0,saved_receiver_len);
		strcpy(saved_receiver, buf);
	memset(&buf,0,buf_len);
	sprintf(buf, "Success\n");
	successusername = 1;
	}
	else{
	memset(&buf,0,buf_len);
	sprintf(buf, "This username does not exist\n");
	    }
        }

	Encrypt(buf, client_id);
	msg_len = strlen(buf);
	
	bytes_sent=0;
        while ( bytes_sent < msg_len) {
            ret = send(socket_desc, buf + bytes_sent, msg_len - bytes_sent, 0);
            if (ret == -1 && errno == EINTR) continue;
            if (ret == -1) handle_error("Cannot write to the socket");
            bytes_sent += ret;
        	}

	if(successusername == 1) break;
	else{
	printf("[CLIENT #%u] Starting over with user receiver...\n", client_id+1);
	sleep(1);
	}
    }

    while(1){
    //svuoto la chat temporanea
    chat_content.erase (chat_content.begin(), chat_content.end());

    //cerco il nome della chat basandomi sul nome loggato e quello scelto del ricevente

    std::string chat_file_name = getchatname(usernamecheck, saved_receiver);

    std::cout << "[CLIENT #" << client_id+1 <<"] Opening chat: '" << chat_file_name << "'\n" << endl;
    
    //leggo il contenuto della chat nella struct

    chat_content = getChat(chat_file_name);

    sleep(1);

    if(chat_content == "Error"){
    	printf("[CLIENT #%u] Could not return chat_content something went wrong...\n", client_id+1);
	goto L;
    }
    else{
    std::cout << "[CLIENT #" << client_id+1 << "]What is in the current chat: '" << chat_file_name << "' is:\n" << chat_content << endl; 
    break;
        }
    }

    //Dopo aver letto la chat la invio come primo messaggio al client

        //std::cout << "TEST CHAT CONTENT 2:\n" << chat_content << endl; 
    	memset(&buf,0,buf_len);
	strcpy(buf, chat_content.c_str());
	//fprintf(stderr, "Prova contenuto buffer:\n%s", buf);

	Encrypt(buf, client_id);	

	msg_len = strlen(buf);

	bytes_sent=0;
        while ( bytes_sent < msg_len) {
            ret = send(socket_desc, buf + bytes_sent, msg_len - bytes_sent, 0);
            if (ret == -1 && errno == EINTR) continue;
            if (ret == -1) handle_error("Cannot write to the socket");
            bytes_sent += ret;
        }

	if (DEBUG) fprintf(stderr, "[CLIENT #%u] Sent chat of %d bytes...\n", client_id+1, bytes_sent);	
	
    //Mi salvo la lunghezza della chat
    chat_length = chat_content.length();
    sleep(1);

    //Loop della chat
    while(1){

	//Rileggo la chat
    	if (chat_content != getChat(getchatname(usernamecheck, saved_receiver))){
		 chat_content = getChat(getchatname(usernamecheck, saved_receiver));
	}

	memset(&buf,0,buf_len);
	chat_length_temp = chat_content.length();
	strcpy(buf, chat_content.c_str());
	Encrypt(buf, client_id);
	msg_len = strlen(buf);

	//printf("PROVA BUF with length %d:\n%s\n", msg_len, buf);

	// DEVO MANDARE LA LUNGHEZZA AL CLIENT PER FARE IL CONFRONTO
	bytes_sent=0;
        while ( bytes_sent < msg_len) {
            ret = send(socket_desc, buf + bytes_sent, msg_len - bytes_sent, 0);
            if (ret == -1 && errno == EINTR) continue;
            if (ret == -1) handle_error("Cannot write to the socket");
            bytes_sent += ret;
        }

	memset(&buf,0,buf_len);

	printf("\n[CLIENT #%u] Sent chat length of %d bytes...\n\n", client_id+1, bytes_sent);

	printf("[CLIENT #%u] Checking chat length...\n", client_id+1);
	if(chat_length < chat_length_temp){
	printf("[CLIENT #%u] Chat has been updated, resending...\n", client_id+1);
		sleep(1);
		strcpy(buf, chat_content.substr(chat_length,chat_length_temp).c_str());

		//printf("\n\nDENTRO IL BUFFER CI STA :\n'%s'\n\n", buf);

		Encrypt(buf, client_id);
		msg_len = strlen(buf);

		bytes_sent=0;
        	while ( bytes_sent < msg_len) {
            		ret = send(socket_desc, buf + bytes_sent, msg_len - bytes_sent, 0);
            		if (ret == -1 && errno == EINTR) continue;
            		if (ret == -1) handle_error("Cannot write to the socket");
            		bytes_sent += ret;
        	}

		if (DEBUG) fprintf(stderr, "[CLIENT #%u] Sent new message of %d bytes...\n", client_id+1, bytes_sent);	
		chat_length = chat_content.length();
		memset(&buf,0,buf_len);		
	}
	else{
	printf("[CLIENT #%u] Chat untouched...\n", client_id+1);
	}

	//ricevo il messaggio
	recv_bytes = 0;
        do {
            ret = recv(socket_desc, buf + recv_bytes, 1, 0);
            if (ret == -1 && errno == EINTR) continue;
            if (ret == -1) handle_error("Cannot read from the socket");
            if (ret == 0) break;
	   } while ( buf[recv_bytes++] != '\n' );

	Decrypt(buf, client_id);

	printf("[CLIENT #%u] Received in chat: \n%s", client_id+1, buf);


	//controllo se il client ha mandato stringa vuota per sapere se ci sono altri messaggi
	std::string check_if_nothing = RemoveNewLine(usernamecheck);
	check_if_nothing += ": \n";

        //cout << "Check if nothing:\n" << check_if_nothing;
	//printf("Buffer:\n%s", buf);

	if ((is_nothing(buf, check_if_nothing)) != true){

	if (recv_bytes == (int)quit_command_len && !memcmp(buf, quit_command, quit_command_len)){

        	if (DEBUG) fprintf(stderr, "[CLIENT #%u] Received QUIT command...\n", client_id+1);
    		std::string chat_file_name = getchatname(usernamecheck, saved_receiver);

		save_chat_on_file(chat_file_name, chat_content, client_id+1);

             	break;
        }

	chat_content+=buf;
	temp_chat_content = chat_content;

	//cout << "\nCOSA C'E' NELLA CHAT DA SALVARE NELLA STRUCT:\n" << chat_content;

	//metto il contenuto nella struct e nel file

	save_chat_on_file(getchatname(usernamecheck, saved_receiver), chat_content, client_id+1);
	save_chat_on_struct(chat_content, getchatname(usernamecheck, saved_receiver), client_id+1);

	sleep(1);
	}
	else{
	printf("[CLIENT #%u] Received nothing to update current chat...\n", client_id+1);
	sleep(1);}
    }

L:
	
    ret = close(socket_desc);
    if(ret) handle_error("Errore nella chiusura della socket");

    free(arg);
    
    master_client_id--;

    printf("[CLIENT #%u] Thread connection_handler terminato\n", client_id+1);
    pthread_exit(NULL);
}

int main(int argc, char* argv[]) {

    // Inizializzo il database struct per la prima volta

    std::string User[USERNAME_SIZE] = {"andrea", "paolo", "leonardo", "niccolo", "sara"};
    std::string Passwords[USERNAME_SIZE] = {"admin", "secheteio", "macchinine", "ululare", "secondo"};

    printf("Putting users and password in struct database");
    InitializeUsers(User,Passwords);
    printf("\nPutting chat files in struct database");
    InitializeChats(Chats);
    printf("\n");

    //Da qua in poi faccio partire il server
    int ret;
    int i;

    //Creo i semafori
    for(i = 0; i < N; i++) {
       ret = sem_init(&semaphores[i], 0, 1);
       if(ret) handle_error("Errore nell’inizializzazione del semaforo");
    }
    
    // creo le strutture per la socket
    int client_desc;
    struct sockaddr_in server_addr = {0};
    
    int sockaddr_len = sizeof(struct sockaddr_in); // da riusare per le accept()
    
    // inizializzo la socket
    master_socket = socket(AF_INET , SOCK_STREAM , 0);
    if(master_socket == -1) handle_error("Impossibile creare la socket");
    
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_family      = AF_INET;
    server_addr.sin_port        = htons(SERVER_PORT);
    
    // abilito l'opzione SO_REUSEADDR per riavviare il server dopo un crash
    int reuseaddr_opt = 1;
    ret = setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, &reuseaddr_opt, sizeof(reuseaddr_opt));
    if(ret) handle_error("Impossibile settare l'opzione SO_REUSEADDR");
    
    // bind & listen
    ret = bind(master_socket, (struct sockaddr*) &server_addr, sockaddr_len);
    if(ret) handle_error("Impossibile fare il binding indirizzo-socket");
    
    ret = listen(master_socket, 16);
    if(ret) handle_error("Impossibile ascoltare dalla socket");
        
    // loop per gestire connessioni in ingresso con nuovi thread
    printf("Server ready to accept connections!\n");
    
    // handling segnali CTRL+C/Z
    signal(SIGINT, signal_callback_handler);
    signal(SIGTSTP, signal_callback_handler);

    while (1) { 

        pthread_t thread;
        //alloco un buffer client_addr per la connessione
        struct sockaddr_in* client_addr = (struct sockaddr_in *)calloc(1, sizeof(struct sockaddr_in));
    
        // accetto connessioni in ingresso
        client_desc = accept(master_socket, (struct sockaddr*) client_addr, (socklen_t*) &sockaddr_len);
        if (client_desc == -1 && errno == EINTR) continue; // check per segnali di interruzione
        if (client_desc == -1) handle_error("Impossibile aprire la socket per connessioni in ingresso.");
    	printf("\nUpdating Chats");
    	InitializeChats(Chats);
    	printf("\n\n");
        printf("Connection accepted!\n");
	

	//Thread args
        handler_args_t* arg = (handler_args_t *)calloc(1, sizeof(handler_args_t));
        arg->socket_desc = client_desc;
        arg->client_addr = client_addr;
        arg->client_id = master_client_id;
        ret = pthread_create(&thread, NULL, connection_handler, arg);
        if(ret) handle_error_en(ret,"Errore nella creazione del thread");
        
        ret = pthread_detach(thread);
        if(ret) handle_error_en(ret,"Errore nel detach del thread");
        
        // incremento il client ID dopo ogni connessione accettata
        master_client_id++;
    }

    return 0;
}
