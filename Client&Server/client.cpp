#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>  // htons() and inet_addr()
#include <netinet/in.h> // struct sockaddr_in
#include <sys/socket.h>
#include <string>
#include <iostream>
#include <algorithm>
#include <signal.h> 

#include "common.h"

using namespace std;

//dichiaro la qui la variabile usata per la socket per poterla chiudere anche da signal_handler
int socket_desc;

//questa funzione mi elimina la prima occorrenza della stringa toErase da response
std::string eraseSubStr(std::string response, std::string toErase)
{
    response.erase(std::remove(response.begin(), response.end(), '\n'), response.end());
    toErase.erase(std::remove(toErase.begin(), toErase.end(), '\n'), toErase.end());

    std::string mainStr = response;
    // Search for the substring in string
    size_t pos = mainStr.find(toErase);
    if (pos != std::string::npos)
    {
        // If found then erase it from string
        mainStr.erase(pos, toErase.length());
    }
    return mainStr;
}

//questa funzione mi elimina ogni occorrenza della stringa toErase da response e la uso solo nella prima recezione di tutta la chat
std::string eraseSubStrFirstChat(std::string response, std::string toErase)
{
    std::string mainStr = response;
    // Search for the substring in string
    size_t pos = mainStr.find(toErase);
    while ((pos  = mainStr.find(toErase) )!= std::string::npos)
    {
        // If found then erase it from string
        mainStr.erase(pos, toErase.length());
    }
    return mainStr;
}

//questa funzione mi consente di prendere il buffer come string senza il \n finale
std::string getBuf(std::string buf){
    buf.erase(std::remove(buf.begin(), buf.end(), '\n'), buf.end());
    return buf;
}

//questa funzione mi permette di creare il messaggio da mandare nella chat nel formato "username: messaggio" sotto forma di string
std::string ComposeMessage(std::string buf, std::string stored_username){
    buf.erase(std::remove(buf.begin(), buf.end(), '\n'), buf.end());
    return stored_username+buf+"\n";   
}

void Encrypt(char *str){
int msg_len = strlen(str);
//printf("\nLEN ENCRYPT: %d\n", msg_len);
for(int i = 0; (i < msg_len-1); i++)
            str[i] = str[i] + 2; //the key for encryption is 3 that is added to ASCII value

         //cout << "\nEncrypted string: " << str << endl;
}

void Decrypt(char *str){
int msg_len = strlen(str);
for(int i = 0; (i < msg_len-1); i++)
         str[i] = str[i] - 2; //the key for encryption is 3 that is subtracted to ASCII value

      //cout << "\nDecrypted string: " << str << endl;
}

void signal_callback_handler(int signum) {
	int ret, msg_len;
	char signal_buf [50];
	sprintf(signal_buf, (char*)SERVER_COMMAND);
	
	Encrypt(signal_buf);

	msg_len = strlen(signal_buf);

	int bytes_sent=0;
        	while ( bytes_sent < msg_len) {
        	    ret = send(socket_desc, signal_buf + bytes_sent, msg_len - bytes_sent, 0);
         	    if (ret == -1 && errno == EINTR) continue;
         	    if (ret == -1) handle_error("Cannot write to the socket");
            	bytes_sent += ret;
        	}

	cout << "\nClient closed by signal handler, Bye! :)\n";
	//chiusura socket
	ret = close(socket_desc);
	if (ret) handle_error ("Errore chiusura socket");
	exit(signum);
}

int main(int argc, char* argv[]) {
    int ret,bytes_sent,recv_bytes;

    // variabili per gestire la socket
    struct sockaddr_in server_addr = {0}; // alcuni campi richiedono di essere riempiti con 0

    socket_desc = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_desc < 0)
        handle_error("Could not create socket");

    //if (DEBUG) fprintf(stderr, "Socket created...\n");

    server_addr.sin_addr.s_addr = inet_addr(SERVER_ADDRESS);
    server_addr.sin_family      = AF_INET;
    server_addr.sin_port        = htons(SERVER_PORT); 

    // inizializza la connessione alla socket
    ret = connect(socket_desc, (struct sockaddr*) &server_addr, sizeof(struct sockaddr_in));
    if (ret < 0)
        handle_error("Could not create connection");

    //if (DEBUG) fprintf(stderr, "Connection established!\n\n");

    char buf[1024];
    char temp_buf[1024];
    std::string stored_username = "";
    size_t buf_len = sizeof(buf);
    size_t temp_buf_len = sizeof(temp_buf);
    int msg_len;
    int chat_length, chat_length_temp;
    std::string first_chat;

    char* quit_command = (char*)SERVER_COMMAND;
    size_t quit_command_len = strlen(quit_command);

    //questo valore lo uso come controllo nel loop della chat così la prima volta non mi mostra che non ci sono nuovi messaggi
    int is_the_first_time = 0;

    // handling segnali CTRL+C/Z
    signal(SIGINT, signal_callback_handler);
    signal(SIGTSTP, signal_callback_handler);

    memset(buf,0,buf_len);


    //Ricevo il messaggio di benvenuto
    recv_bytes = 0;
    do {
        ret = recv(socket_desc, buf + recv_bytes, buf_len - recv_bytes, 0);
        if (ret == -1 && errno == EINTR) continue;
        if (ret == -1) handle_error("Cannot read from the socket");
        if (ret == 0) break;
	recv_bytes += ret;

    } while ( buf[recv_bytes-1] != '\n' );

    printf("%s\n", buf);

 
    //Login Loop//
    while(1) {
        memset(&buf,0,buf_len);
	fprintf(stderr, "Insert Your Username:\n");
	
	
	if (fgets(buf, sizeof(buf), stdin) != (char*)buf) {
            fprintf(stderr, "Error while reading from stdin, exiting...\n");
            exit(EXIT_FAILURE);
        }

	stored_username = getBuf(buf);
	stored_username += ": ";

	//cout << "\nPROVA: " << stored_username;

	Encrypt(buf);

        msg_len = strlen(buf);

        bytes_sent=0;
        while ( bytes_sent < msg_len) {
            ret = send(socket_desc, buf + bytes_sent, msg_len - bytes_sent, 0);
            if (ret == -1 && errno == EINTR) continue;
            if (ret == -1) handle_error("Cannot write to the socket");
            bytes_sent += ret;
        }

        //if (DEBUG) fprintf(stderr, "Sent message of %d bytes...\n", bytes_sent);

	Decrypt(buf);

	if (msg_len == (int)quit_command_len && !memcmp(buf, quit_command, quit_command_len)){

        if (DEBUG) fprintf(stderr, "Sent QUIT command ...\n");
             goto L;
        }

        if (DEBUG) fprintf(stderr, "\n");

	recv_bytes = 0;
    	do {
            ret = recv(socket_desc, buf + recv_bytes, buf_len - recv_bytes, 0);
            if (ret == -1 && errno == EINTR) continue;
            if (ret == -1) handle_error("Cannot read from the socket");
	    if (ret == 0) break;
	    recv_bytes += ret;

	} while ( buf[recv_bytes-1] != '\n' );

        //if (DEBUG) fprintf(stderr, "Received answer of %d bytes...\n",recv_bytes);

	Decrypt(buf);

	if (strcmp(buf, "Insert your password:\n") == 0){

		printf("%s", buf);
		
		memset(&buf,0,buf_len);

		if (fgets(buf, sizeof(buf), stdin) != (char*)buf) {
          	  fprintf(stderr, "Error while reading from stdin, exiting...\n");
          	  exit(EXIT_FAILURE);
        		}
		
		Encrypt(buf);

		msg_len = strlen(buf);

		bytes_sent=0;
        	while ( bytes_sent < msg_len) {
        	    ret = send(socket_desc, buf + bytes_sent, msg_len - bytes_sent, 0);
         	    if (ret == -1 && errno == EINTR) continue;
         	    if (ret == -1) handle_error("Cannot write to the socket");
            	bytes_sent += ret;
        	}

	       Decrypt(buf);

	       if (msg_len == (int)quit_command_len && !memcmp(buf, quit_command, quit_command_len)){

               if (DEBUG) fprintf(stderr, "Sent QUIT command ...\n");
                   goto L;
               }		

		recv_bytes = 0;
    		do {
            	ret = recv(socket_desc, buf + recv_bytes, buf_len - recv_bytes, 0);
            	if (ret == -1 && errno == EINTR) continue;
            	if (ret == -1) handle_error("Cannot read from the socket");
	    	if (ret == 0) break;
	    	recv_bytes += ret;

		} while ( buf[recv_bytes-1] != '\n' );

		Decrypt(buf);

		printf("\n%s\n", buf); 
		if (strcmp(buf, "Login Successful\n") == 0) break;
		
		
	
		}
	else {
	printf("Retry please...\n\n");
		}
	}
  
	//Controllo a chi vuoi scrivere
	
	while(1){
	memset(&buf,0,buf_len);
	
	recv_bytes = 0;
    		do {
            	ret = recv(socket_desc, buf + recv_bytes, buf_len - recv_bytes, 0);
            	if (ret == -1 && errno == EINTR) continue;
            	if (ret == -1) handle_error("Cannot read from the socket");
	    	if (ret == 0) break;
	    	recv_bytes += ret;

	} while ( buf[recv_bytes-1] != '\n' );	

	Decrypt(buf);

	printf("\n%s", buf);

	//inserisco il nome del destinatario
	if (fgets(buf, sizeof(buf), stdin) != (char*)buf) {
          	  fprintf(stderr, "Error while reading from stdin, exiting...\n");
          	  exit(EXIT_FAILURE);
        		}
		
	Encrypt(buf);

	msg_len = strlen(buf);

	bytes_sent=0;
        while ( bytes_sent < msg_len) {
        	   ret = send(socket_desc, buf + bytes_sent, msg_len - bytes_sent, 0);
         	   if (ret == -1 && errno == EINTR) continue;
         	   if (ret == -1) handle_error("Cannot write to the socket");
            bytes_sent += ret;
        }

	Decrypt(buf);

	//controllo se ho scritto QUIT
	if (msg_len == (int)quit_command_len && !memcmp(buf, quit_command, quit_command_len)){

        if (DEBUG) fprintf(stderr, "Sent QUIT command ...\n");
              goto L;
        }		

	memset(&buf,0,buf_len);
	recv_bytes = 0;
    		do {
            	ret = recv(socket_desc, buf + recv_bytes, buf_len - recv_bytes, 0);
            	if (ret == -1 && errno == EINTR) continue;
            	if (ret == -1) handle_error("Cannot read from the socket");
	    	if (ret == 0) break;
	    	recv_bytes += ret;

	} while ( buf[recv_bytes-1] != '\n' );	

	Decrypt(buf);

	if(strcmp(buf, "Success\n") == 0 ){
	break;
	}
	else{
	printf("\n%s\n", buf);	
	printf("Starting over...\n");
	}
}

	//chat stuff...
	//Receive chat for first time
	memset(&buf,0,buf_len);
	recv_bytes = 0;
    		do {
            	ret = recv(socket_desc, buf + recv_bytes, buf_len - recv_bytes, 0);
            	if (ret == -1 && errno == EINTR) continue;
            	if (ret == -1) handle_error("Cannot read from the socket");
	    	if (ret == 0) break;
	    	recv_bytes += ret;

	} while ( buf[recv_bytes-1] != '\n' );	

	Decrypt(buf);

	chat_length = recv_bytes;
	chat_length_temp = chat_length;

	printf("\n[INFO] Waiting for the chat to load please wait...\n");

	printf("\n[REMEMBER] To check for new messages press enter, ALWAYS check before close...\n");

	sleep(2);

	//levo il "username: " dai messaggi così si mescola meglio con il terminale
	first_chat = eraseSubStrFirstChat(buf, stored_username);

	cout << "\n" << first_chat;

	//printf("\n%s", buf);

	//printf("Chat length is %d\n", chat_length);
	

   while(1){
	memset(&buf,0,buf_len);
	//printf("\nCHAT LENGTH START: %d\n", chat_length_temp);
	//printf("\nChecking if chat has been updated...\n");
	
	//Prima devo ricevere la lunghezza aggiornata
	recv_bytes = 0;
    		do {
            	ret = recv(socket_desc, buf + recv_bytes, buf_len - recv_bytes, 0);
            	if (ret == -1 && errno == EINTR) continue;
            	if (ret == -1) handle_error("Cannot read from the socket");
	    	if (ret == 0) break;
	    	recv_bytes += ret;

	} while ( buf[recv_bytes-1] != '\n' );	

	Decrypt(buf);

	sleep(1);

	chat_length_temp = recv_bytes;

	//printf("\n\nRicevuta lunghezza di chat di %d bytes...\n\n", chat_length_temp);

	memset(&buf,0,buf_len);
	//printf("\n\n\nPROVAAAA: %d\n\n\n", chat_length_temp);
	if(chat_length_temp > chat_length){
		//printf("SONO ENTRATO DENTRO L'IF\n");
		//sleep(1);
		recv_bytes = 0;
    			do {
            		ret = recv(socket_desc, buf + recv_bytes, buf_len - recv_bytes, 0);
            		if (ret == -1 && errno == EINTR) continue;
            		if (ret == -1) handle_error("Cannot read from the socket");
	    		if (ret == 0) break;
	    		recv_bytes += ret;

		} while ( buf[recv_bytes-1] != '\n' );	
		//riceve i messaggi aggiunti
		//printf("SONO DOPO LA RECV NELL'IF\n");
		Decrypt(buf);		

		std::string response = eraseSubStr(buf,temp_buf);
	
		if((int) response.length() != 0){
		cout << "New messagges:\n\n" << response << "\n\n";
			}
			
		chat_length = chat_length_temp;
		memset(&buf,0,buf_len);
		memset(&temp_buf,0,temp_buf_len);
	}
	else if (chat_length_temp == chat_length && is_the_first_time == 1){printf("No new messages to display...\n\n");}

	//Invio il messaggio alla chat

	if (fgets(buf, sizeof(buf), stdin) != (char*)buf) {
          	  fprintf(stderr, "Error while reading from stdin, exiting...\n");
          	  exit(EXIT_FAILURE);
        		}

	msg_len = strlen(buf);


	//controllo uscita
	if (msg_len == (int)quit_command_len && !memcmp(buf, quit_command, quit_command_len)){

		Encrypt(buf);

		bytes_sent=0;
        	while ( bytes_sent < msg_len) {
        	   ret = send(socket_desc, buf + bytes_sent, msg_len - bytes_sent, 0);
         	   if (ret == -1 && errno == EINTR) continue;
         	   if (ret == -1) handle_error("Cannot write to the socket");
            	bytes_sent += ret;
        	}

        	if (DEBUG) fprintf(stderr, "Sent QUIT command ...\n");
              	break;
        }
	else{


	//vedo se ha premuto invio
	if(strcmp(buf,"\n") == 0) printf("Checking for new messages...\n"); 

	std::string message_sent = ComposeMessage(buf, stored_username);

        strcpy(buf, message_sent.c_str());

	Encrypt(buf);

	msg_len = strlen(buf);

	bytes_sent=0;
        while ( bytes_sent < msg_len) {
        	   ret = send(socket_desc, buf + bytes_sent, msg_len - bytes_sent, 0);
         	   if (ret == -1 && errno == EINTR) continue;
         	   if (ret == -1) handle_error("Cannot write to the socket");
            bytes_sent += ret;
        }

	Decrypt(buf);

	if(strcmp(buf,"\n") != 0) strcpy(temp_buf, buf);
	
	//printf("\nCHAT LENGTH FINISH: %d\n", chat_length_temp);  

	if(is_the_first_time != 1) is_the_first_time = 1;
 
	sleep(1);
		}
	}
	

L:
    ret = close(socket_desc);

    if (ret < 0) handle_error("Cannot close the socket");

    //if (DEBUG) fprintf(stderr, "Socket closed...\n");

    if (DEBUG) fprintf(stderr, "\nBye! :)\n");

    exit(EXIT_SUCCESS);
}
