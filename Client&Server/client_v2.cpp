#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>  // htons() and inet_addr()
#include <netinet/in.h> // struct sockaddr_in
#include <sys/socket.h>
#include <sys/select.h>
#include <string>
#include <iostream>
#include <algorithm>
#include <signal.h> 
#include <pthread.h>

#include "common.h"

using namespace std;

//dichiaro la qui la variabile usata per la socket per poterla chiudere anche da signal_handler
int socket_desc;
int interrupt_chat = 0; //mi serve per chiudere entrambi i thread per la send e la recv
char temp_buf [500]; // lo uso nei thread send e recv per evitare di rimandarmi il messaggio appena inviato

//struttura thread
typedef struct chat_args{
    int socket_desc;
    std::string stored_username;
} chat_args_t;

//questa funzione mi elimina la prima occorrenza della stringa toErase da response
std::string eraseSubStr(std::string response, std::string toErase)
{
    response.erase(std::remove(response.begin(), response.end(), '\n'), response.end());
    toErase.erase(std::remove(toErase.begin(), toErase.end(), '\n'), toErase.end());

    std::string mainStr = response;
    // Search for the substring in string
    size_t pos = mainStr.find(toErase);
    if (pos != std::string::npos)
    {
        // If found then erase it from string
        mainStr.erase(pos, toErase.length());
    }
    return mainStr;
}

//questa funzione mi elimina ogni occorrenza della stringa toErase da response e la uso solo nella prima recezione di tutta la chat
std::string eraseSubStrFirstChat(std::string response, std::string toErase)
{
    std::string mainStr = response;
    // Search for the substring in string
    size_t pos = mainStr.find(toErase);
    while ((pos  = mainStr.find(toErase) )!= std::string::npos)
    {
        // If found then erase it from string
        mainStr.erase(pos, toErase.length());
    }
    return mainStr;
}

//questa funzione mi consente di prendere il buffer come string senza il \n finale
std::string getBuf(std::string buf){
    buf.erase(std::remove(buf.begin(), buf.end(), '\n'), buf.end());
    return buf;
}

//questa funzione mi permette di creare il messaggio da mandare nella chat nel formato "username: messaggio" sotto forma di string
std::string ComposeMessage(std::string buf, std::string stored_username){
    buf.erase(std::remove(buf.begin(), buf.end(), '\n'), buf.end());
    return stored_username+buf+"\n";   
}

void Encrypt(char *str){
int msg_len = strlen(str);
//printf("\nLEN ENCRYPT: %d\n", msg_len);
for(int i = 0; (i < msg_len-1); i++)
            str[i] = str[i] + 2; //the key for encryption is 3 that is added to ASCII value

         //cout << "\nEncrypted string: " << str << endl;
}

void Decrypt(char *str){
int msg_len = strlen(str);
for(int i = 0; (i < msg_len-1); i++)
         str[i] = str[i] - 2; //the key for encryption is 3 that is subtracted to ASCII value

      //cout << "\nDecrypted string: " << str << endl;
}

void signal_callback_handler(int signum) {
	int ret, msg_len;
	char signal_buf [50];
	sprintf(signal_buf, (char*)SERVER_COMMAND);
	
	Encrypt(signal_buf);

	msg_len = strlen(signal_buf);

	int bytes_sent=0;
        	while ( bytes_sent < msg_len) {
        	    ret = send(socket_desc, signal_buf + bytes_sent, msg_len - bytes_sent, 0);
         	    if (ret == -1 && errno == EINTR) continue;
         	    if (ret == -1) handle_error("Cannot write to the socket");
            	bytes_sent += ret;
        	}

	cout << "\nClient closed by signal handler, Bye! :)\n";
	//chiusura socket
	ret = close(socket_desc);
	if (ret) handle_error ("Errore chiusura socket");
	interrupt_chat = 1;
	exit(signum);
}


//thread funzioni
void* send_chat(void* chat_args){
    int ret, bytes_sent;

    chat_args_t* arg_send = (chat_args_t*)chat_args;
    int socket_desc = arg_send->socket_desc;
    std::string stored_username = arg_send->stored_username;
    char buf [500];
    int msg_len;

    char* quit_command = (char*)SERVER_COMMAND;
    size_t quit_command_len = strlen(quit_command);

    while(interrupt_chat == 0){
	memset(&buf, 0 ,sizeof(buf));

		fgets(buf, sizeof(buf), stdin);

		msg_len = strlen(buf);

		//controllo uscita
		if (msg_len == (int)quit_command_len && !memcmp(buf, quit_command, quit_command_len)){

			Encrypt(buf);

			bytes_sent=0;
        		while ( bytes_sent < msg_len) {
        	   	ret = send(socket_desc, buf + bytes_sent, msg_len - bytes_sent, 0);
         	   	if (ret == -1 && errno == EINTR) continue;
         	   	if (ret == -1) handle_error("Cannot write to the socket");
            		bytes_sent += ret;
        		}

        	if (DEBUG) fprintf(stderr, "Sent QUIT command ...\n");
		interrupt_chat = 1;
              	break;
        	}
		
		if(strcmp(buf,"\n") != 0){

		std::string message_sent = ComposeMessage(buf, stored_username);

        	strcpy(buf, message_sent.c_str());
		}

    		strcpy(temp_buf, buf);

		Encrypt(buf);

		msg_len = strlen(buf);

		bytes_sent=0;
		while ( bytes_sent < msg_len) {
        	ret = send(socket_desc, buf + bytes_sent, msg_len - bytes_sent, 0);
        	if (ret == -1 && errno == EINTR) continue;
        	if (ret == -1) handle_error("Cannot write to the socket");
        	bytes_sent += ret;
    		}

		sleep(1);
	    
	}

    pthread_exit(NULL);
}

void* recv_chat(void* chat_args){
    int ret, recv_bytes;

    chat_args_t* arg_recv = (chat_args_t*)chat_args;
    int socket_desc = arg_recv->socket_desc;
    std::string stored_username = arg_recv->stored_username;

    std::string response;
    char buf [500];

    while(interrupt_chat == 0){
    memset(&buf, 0, sizeof(buf));

    recv_bytes = 0;
    	do {
            ret = recv(socket_desc, buf + recv_bytes, sizeof(buf) - recv_bytes, 0);
            if (ret == -1 && errno == EINTR) continue;
            if (ret == -1) handle_error("Cannot read from the socket");
	    if (ret == 0) break;
	    recv_bytes += ret;

	} while ( buf[recv_bytes-1] != '\n' );

    Decrypt(buf);

    response = eraseSubStr(buf, temp_buf);
    memset(&temp_buf, 0, sizeof(temp_buf));

    //in questo modo stampo solo se ricevo messaggi da parte dell'altro interlocutore
    if((int) response.length() != 0){
    cout << response << endl;
      }

    }

    free(chat_args);
    pthread_exit(NULL);
}



int main(int argc, char* argv[]) {
    int ret,bytes_sent,recv_bytes;


    // variabili per gestire la socket
    struct sockaddr_in server_addr = {0}; // alcuni campi richiedono di essere riempiti con 0

    socket_desc = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_desc < 0)
        handle_error("Could not create socket");
    //if (DEBUG) fprintf(stderr, "Socket created...\n");

    server_addr.sin_addr.s_addr = inet_addr(SERVER_ADDRESS);
    server_addr.sin_family      = AF_INET;
    server_addr.sin_port        = htons(SERVER_PORT); 

    // inizializza la connessione alla socket
    ret = connect(socket_desc, (struct sockaddr*) &server_addr, sizeof(struct sockaddr_in));
    if (ret < 0)
        handle_error("Could not create connection");

    //if (DEBUG) fprintf(stderr, "Connection established!\n\n");

    char buf[1024];
    std::string stored_username = "";
    size_t buf_len = sizeof(buf);
    int msg_len;
    std::string first_chat;

    //Thread Send e Recv
    pthread_t chat_threads[2];
    chat_args_t* chat_args = (chat_args_t *)calloc(1, sizeof(chat_args_t));

    char* quit_command = (char*)SERVER_COMMAND;
    size_t quit_command_len = strlen(quit_command);


    // handling segnali CTRL+C/Z
    signal(SIGINT, signal_callback_handler);
    signal(SIGTSTP, signal_callback_handler);

    memset(buf,0,buf_len);


    //Ricevo il messaggio di benvenuto
    recv_bytes = 0;
    do {
        ret = recv(socket_desc, buf + recv_bytes, buf_len - recv_bytes, 0);
        if (ret == -1 && errno == EINTR) continue;
        if (ret == -1) handle_error("Cannot read from the socket");
        if (ret == 0) break;
	recv_bytes += ret;

    } while ( buf[recv_bytes-1] != '\n' );

    printf("%s\n", buf);

 
    //Login Loop//
    while(1) {
        memset(&buf,0,buf_len);
	fprintf(stderr, "Insert Your Username:\n");
	
	
	if (fgets(buf, sizeof(buf), stdin) != (char*)buf) {
            fprintf(stderr, "Error while reading from stdin, exiting...\n");
            exit(EXIT_FAILURE);
        }

	stored_username = getBuf(buf);
	stored_username += ": ";

	//cout << "\nPROVA: " << stored_username;

	Encrypt(buf);

        msg_len = strlen(buf);

        bytes_sent=0;
        while ( bytes_sent < msg_len) {
            ret = send(socket_desc, buf + bytes_sent, msg_len - bytes_sent, 0);
            if (ret == -1 && errno == EINTR) continue;
            if (ret == -1) handle_error("Cannot write to the socket");
            bytes_sent += ret;
        }

        //if (DEBUG) fprintf(stderr, "Sent message of %d bytes...\n", bytes_sent);

	Decrypt(buf);

	if (msg_len == (int)quit_command_len && !memcmp(buf, quit_command, quit_command_len)){

        if (DEBUG) fprintf(stderr, "Sent QUIT command ...\n");
             goto L;
        }

        if (DEBUG) fprintf(stderr, "\n");

	recv_bytes = 0;
    	do {
            ret = recv(socket_desc, buf + recv_bytes, buf_len - recv_bytes, 0);
            if (ret == -1 && errno == EINTR) continue;
            if (ret == -1) handle_error("Cannot read from the socket");
	    if (ret == 0) break;
	    recv_bytes += ret;

	} while ( buf[recv_bytes-1] != '\n' );

        //if (DEBUG) fprintf(stderr, "Received answer of %d bytes...\n",recv_bytes);

	Decrypt(buf);

	if (strcmp(buf, "Insert your password:\n") == 0){

		printf("%s", buf);
		
		memset(&buf,0,buf_len);

		if (fgets(buf, sizeof(buf), stdin) != (char*)buf) {
          	  fprintf(stderr, "Error while reading from stdin, exiting...\n");
          	  exit(EXIT_FAILURE);
        		}
		
		Encrypt(buf);

		msg_len = strlen(buf);

		bytes_sent=0;
        	while ( bytes_sent < msg_len) {
        	    ret = send(socket_desc, buf + bytes_sent, msg_len - bytes_sent, 0);
         	    if (ret == -1 && errno == EINTR) continue;
         	    if (ret == -1) handle_error("Cannot write to the socket");
            	bytes_sent += ret;
        	}

	       Decrypt(buf);

	       if (msg_len == (int)quit_command_len && !memcmp(buf, quit_command, quit_command_len)){

               if (DEBUG) fprintf(stderr, "Sent QUIT command ...\n");
                   goto L;
               }		

		recv_bytes = 0;
    		do {
            	ret = recv(socket_desc, buf + recv_bytes, buf_len - recv_bytes, 0);
            	if (ret == -1 && errno == EINTR) continue;
            	if (ret == -1) handle_error("Cannot read from the socket");
	    	if (ret == 0) break;
	    	recv_bytes += ret;

		} while ( buf[recv_bytes-1] != '\n' );

		Decrypt(buf);

		printf("\n%s\n", buf); 
		if (strcmp(buf, "Login Successful\n") == 0) break;
		
		
	
		}
	else {
	printf("Retry please...\n\n");
		}
	}
  
	//Controllo a chi vuoi scrivere
	
	while(1){
	memset(&buf,0,buf_len);
	
	recv_bytes = 0;
    		do {
            	ret = recv(socket_desc, buf + recv_bytes, buf_len - recv_bytes, 0);
            	if (ret == -1 && errno == EINTR) continue;
            	if (ret == -1) handle_error("Cannot read from the socket");
	    	if (ret == 0) break;
	    	recv_bytes += ret;

	} while ( buf[recv_bytes-1] != '\n' );	

	Decrypt(buf);

	printf("%s", buf);

	//inserisco il nome del destinatario
	if (fgets(buf, sizeof(buf), stdin) != (char*)buf) {
          	  fprintf(stderr, "Error while reading from stdin, exiting...\n");
          	  exit(EXIT_FAILURE);
        		}
		
	Encrypt(buf);

	msg_len = strlen(buf);

	bytes_sent=0;
        while ( bytes_sent < msg_len) {
        	   ret = send(socket_desc, buf + bytes_sent, msg_len - bytes_sent, 0);
         	   if (ret == -1 && errno == EINTR) continue;
         	   if (ret == -1) handle_error("Cannot write to the socket");
            bytes_sent += ret;
        }

	Decrypt(buf);

	//controllo se ho scritto QUIT
	if (msg_len == (int)quit_command_len && !memcmp(buf, quit_command, quit_command_len)){

        if (DEBUG) fprintf(stderr, "Sent QUIT command ...\n");
              goto L;
        }		

	memset(&buf,0,buf_len);
	recv_bytes = 0;
    		do {
            	ret = recv(socket_desc, buf + recv_bytes, buf_len - recv_bytes, 0);
            	if (ret == -1 && errno == EINTR) continue;
            	if (ret == -1) handle_error("Cannot read from the socket");
	    	if (ret == 0) break;
	    	recv_bytes += ret;

	} while ( buf[recv_bytes-1] != '\n' );	

	Decrypt(buf);

	if(strcmp(buf, "Success\n") == 0 ){
	break;
	}
	else{
	printf("\n%s\n", buf);	
	printf("Starting over...\n");
	}
}

	//chat stuff...
	//Receive chat for first time
	memset(&buf,0,buf_len);
	recv_bytes = 0;
    		do {
            	ret = recv(socket_desc, buf + recv_bytes, buf_len - recv_bytes, 0);
            	if (ret == -1 && errno == EINTR) continue;
            	if (ret == -1) handle_error("Cannot read from the socket");
	    	if (ret == 0) break;
	    	recv_bytes += ret;

	} while ( buf[recv_bytes-1] != '\n' );	

	Decrypt(buf);

	//levo il "username: " dai messaggi così si mescola meglio con il terminale
	first_chat = eraseSubStrFirstChat(buf, stored_username);

	cout << "\n" << first_chat;

	//printf("\n%s", buf);

        //Thread argomenti
        chat_args->socket_desc = socket_desc;
        chat_args->stored_username = stored_username;

        ret = pthread_create(&chat_threads[0], NULL, recv_chat, chat_args);
        if(ret) handle_error_en(ret,"Errore nella creazione del thread");

        ret = pthread_create(&chat_threads[1], NULL, send_chat, chat_args);
        if(ret) handle_error_en(ret,"Errore nella creazione del thread");
    
        for (int i = 0; i < 2; i++){
	   ret = pthread_join(chat_threads[i], NULL);
	   if (ret != 0) { fprintf(stderr, "Error %d in pthread_join\n", ret); exit(EXIT_FAILURE); }
        }	

L:
    ret = close(socket_desc);

    if (ret < 0) handle_error("Cannot close the socket");

    //if (DEBUG) fprintf(stderr, "Socket closed...\n");

    if (DEBUG) fprintf(stderr, "\nBye! :)\n");

    exit(EXIT_SUCCESS);
}
